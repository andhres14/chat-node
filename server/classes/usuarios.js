class Usuarios {
    constructor() {
        this.personas = [];
    }

    addPersona(id, name) {
        let person = {id, name};
        this.personas.push(person);
        return this.personas;
    }

    getPersona(id) {
        let persona = this.personas.filter(persona => persona.id === id)[0];
        return persona;
    }

    getPersonas() {
        return this.personas;
    }

    getPersonasPorSala(sala) {

    }

    eliminarPersona(id) {
        let personaBorrada = this.getPersona(id);
        this.personas = this.personas.filter(persona => persona.id !== id)[0];

        return personaBorrada;
    }
}
